# Author: Richard Davis
# rows.py - class to manage row forms

import copy, doctest
from lilypond_note import LilypondNote


class Row:
    """A class representing a row form."""

    def __init__(self, row: list[LilypondNote]) -> None:
        """Construct a Row object.

        >>> f = LilypondNote("f")
        >>> a = LilypondNote("a")
        >>> gs = LilypondNote("gs")
        >>> p = Row([f, a, gs])
        >>> p.row
        [f, a, gs]
        """

        self.row = row
        pass

    def __str__(self) -> str:
        row = copy.deepcopy(self.row)
        string_row = map(str, row)
        return " ".join(string_row)

    def __repr__(self) -> str:
        return self.__str__()

    def __iter__(self):
        return iter(self.row)

    def __getitem__(self, subscript):
        if isinstance(subscript, slice):
            return self.row[subscript.start:subscript.stop:subscript.step]
        else:
            return self.row[subscript]

    def __len__(self):
        return len(self.row)

    def cycle(self):
        """Cycle the row by moving the first note to the end.

        >>> f = LilypondNote("f")
        >>> a = LilypondNote("a")
        >>> gs = LilypondNote("gs")
        >>> p = Row([f, a, gs])
        >>> p.cycle()
        >>> p
        a gs f
        """

        self.row.append(self.row.pop(0))

    def to_cycled(self):
        """Generate a new cycled Row.

        >>> f = LilypondNote("f")
        >>> a = LilypondNote("a")
        >>> gs = LilypondNote("gs")
        >>> p = Row([f, a, gs])
        >>> p.to_cycled()
        a gs f
        """

        cycled_row = copy.deepcopy(self)
        cycled_row.cycle()
        return Row(cycled_row.row)

    def all_cycles(self) -> list:
        """Generate a list of all of the cycles of a Row.

        >>> f = LilypondNote("f")
        >>> a = LilypondNote("a")
        >>> gs = LilypondNote("gs")
        >>> p = Row([f, a, gs])
        >>> p.all_cycles()
        [f a gs, a gs f, gs f a]
        """

        all_cycles = []

        for i in range(len(self)):
            all_cycles.append(copy.deepcopy(self))
            self.cycle()

        return all_cycles

    def transpose(self, interval):
        """Transpose row by interval in semitones.

        >>> f = LilypondNote("f")
        >>> a = LilypondNote("a")
        >>> gs = LilypondNote("gs")
        >>> p1 = Row([f, a, gs])
        >>> p1.transpose(3)
        >>> p1
        af c' b
        """

        for note in self:
            note.transpose(interval)

    def to_transposed(self, interval):
        """Generate a new Row transposed by the given interval.

        >>> f = LilypondNote("f")
        >>> a = LilypondNote("a")
        >>> gs = LilypondNote("gs")
        >>> p1 = Row([f, a, gs])
        >>> p1.to_transposed(3)
        af c' b
        """

        new_row = copy.deepcopy(self)
        new_row.transpose(interval)
        return Row(new_row.row)

    def transpose_to_next(self):
        """Transpose a row to its second note.

        >>> f = LilypondNote("f")
        >>> a = LilypondNote("a")
        >>> gs = LilypondNote("gs")
        >>> p = Row([f, a, gs])
        >>> p.transpose_to_next()
        a cs' c'
        """

        return self.to_transposed(self[1] - self[0])

    def to_pitch_class_row(self):
        """Generate a new Row object in pitch class format.

        >>> f = LilypondNote("f")
        >>> a = LilypondNote("a")
        >>> gs = LilypondNote("gs")
        >>> p = Row([f, a, gs])
        >>> p.to_pitch_class_row()
        f a af
        """

        row = map(lambda x: x.to_pitch_class(), copy.deepcopy(self))
        pitch_class_list = list(row)
        return Row(list(map(LilypondNote.pitch_class_to_lilypond_note, pitch_class_list)))

    def to_sorted_row(self):
        """Generate a new Row that is the sorted version of the original.
        New Row is sorted in ascending order by MIDI pitch.

        >>> f = LilypondNote("f'")
        >>> a = LilypondNote("a")
        >>> gs = LilypondNote("gs")
        >>> p = Row([f, a, gs])
        >>> p.to_sorted_row()
        af a f'
        """

        midi_row = list(map(int, self))
        midi_row.sort()

        row = []

        for i in range(len(midi_row)):
            row.append(LilypondNote.midi_to_lilypond_note(midi_row[i]))

        return Row(row)

    def to_sorted_pitch_class_row(self):
        """Generate a new Row in sorted pitch class format.

        >>> f = LilypondNote("f'")
        >>> a = LilypondNote("a")
        >>> gs = LilypondNote("gs")
        >>> p = Row([f, a, gs])
        >>> p.to_sorted_pitch_class_row()
        f af a
        """

        return self.to_pitch_class_row().to_sorted_row()

    def compare_rows(self, o) -> int:
        """Calculate the total number of semitones of difference (in pitch
        class space) between two rows. Raise a ValueError if row lengths are
        mismatched.

        >>> c = LilypondNote("c")
        >>> cs = LilypondNote("cs")
        >>> f = LilypondNote("f")
        >>> gs = LilypondNote("gs")
        >>> a = LilypondNote("a")
        >>> p1 = Row([f, gs, a])
        >>> p2 = Row([c, a, cs])

        Note: cs and a are 8 semitones apart in pitch space, but 4 in pitch
        class space, so the interval must be counted as 4 semitones.
        >>> p1.compare_rows(p2)
        10

        >>> p3 = Row([f, a, gs, c])
        >>> p1.compare_rows(p3)
        Traceback (most recent call last):
        ValueError: mismatched Row sizes.

        >>> p1.compare_rows(3)
        Traceback (most recent call last):
        TypeError: cannot compare Row to type <class 'int'>.
        """

        if type(self) != type(o):
            raise TypeError(f"cannot compare Row to type {type(o)}.")

        if len(self) != len(o):
            raise ValueError("mismatched Row sizes.")

        semitones_difference = 0

        for i, note in enumerate(self):
            deviation = abs(note - o[i])

            if deviation > 6:
                # get minimum distance between two pitches
                deviation = 12 - deviation % 12

            semitones_difference += deviation

        return semitones_difference

    def common_tones(self, o) -> int:
        """Calculate the number of common tones shared between rows.

        >>> f = LilypondNote("f")
        >>> a = LilypondNote("a")
        >>> gs = LilypondNote("gs")
        >>> b = LilypondNote("b")
        >>> p1 = Row([f, a, gs])
        >>> p2 = Row([f, a, b])
        >>> p1.common_tones(p2)
        2

        >>> p3 = Row([f, a, gs, b])
        >>> p1.compare_rows(p3)
        Traceback (most recent call last):
        ValueError: mismatched Row sizes.

        >>> p1.compare_rows(3)
        Traceback (most recent call last):
        TypeError: cannot compare Row to type <class 'int'>.
        """

        if type(self) != type(o):
            raise TypeError(f"cannot compare Row to type {type(o)}.")

        if len(self) != len(o):
            raise ValueError("mismatched Row sizes.")

        common_tones = 0
        
        for i, note in enumerate(self):
            if note == o[i]:
                common_tones += 1

        return common_tones

    def pop(self, i):
        self.row.pop(i)


if __name__ == "__main__":
    doctest.testmod()
