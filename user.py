# Author: Richard Davis
# user.py - resources for interaction with user

from lilypond_note import LilypondNote, LILYPOND_NOTE_REGEX
from row import Row


def input_lilypond_note(prompt: str = "Enter note: ") -> LilypondNote:
    """Prompt the user for a lilypond note."""
    
    note = input(prompt)

    while not LILYPOND_NOTE_REGEX.match(note):
        print(f'"{note}" not a valid English lilypond note, please input another.')
        note = input(prompt)

    return LilypondNote(note)


def input_lilypond_row(n) -> Row:
    """Prompt the user for a lilypond note n times. Return a lilypond scale."""
    
    lilypond_row = []

    for i in range(n):
        lilypond_row.append(input_lilypond_note(prompt = f"Enter note {i + 1}: "))

    return Row(lilypond_row)


def get_num_notes(message = "How many notes? ") -> int:
    """Interactively get number of notes in the row from user."""
    valid_number = False

    while not valid_number:
        num_notes = input(message)

        if not num_notes.isdigit():  # Check if input is a number
            print("Input not a valid number, please try again.")
        elif int(num_notes) < 1:          # Check if input number is valid
            print("Please enter a number greater than 0.")
        else:
            valid_number = True

    return int(num_notes)
