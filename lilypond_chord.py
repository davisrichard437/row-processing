# Author: Richard Davis
# lilypond_chord.py - LilypondChord class


import copy
import doctest
from lilypond_note import LilypondNote


class LilypondChord:
    """A class to represent a chord in lilypond format."""

    def __init__(self, lilypond_notes: list[LilypondNote]) -> None:
        """Construct a LilypondChord instance.

        >>> f = LilypondNote("f")
        >>> a = LilypondNote("a")
        >>> c = LilypondNote("c'")
        >>> chord = LilypondChord([f, a, c])
        >>> chord
        < f a c' >
        """

        self.chord = lilypond_notes

    def __str__(self) -> str:
        chord = copy.deepcopy(self.chord)
        chord_interior = " ".join(map(str, chord))
        return f"< {chord_interior} >"

    def __repr__(self) -> str:
        return self.__str__()


if __name__ == "__main__":
    doctest.testmod()
