# Author: Richard Davis
# main.py - main file to generate music

# Stuff I need:
# row form class
# Lilypond, midi, pitch class type aliases and operations
# Chords

# Steps:
# 1: Get row
# 2: transpose row
# 3: find closest octaviations of transposed row
# 4: cycle trichords

from lilypond_note import LilypondNote
from lilypond_chord import LilypondChord
from filesystem import increment_filename, print_lilypond_music, FILENAME_FORMAT
from row import Row
from math import lcm
from itertools import permutations
# import copy
import user


def ruth_crawford(row) -> str:
    """Generate a passage of serial music in the style of Ruth Crawford."""

    music = ""

    for i in range(len(row)):
        for r in row.all_cycles():
            music += f"{r}\n"
        row = row.transpose_to_next()

    return music


def transpose_row_to_mapping(
        row1_original: Row,
        row1_sorted: Row,
        row2_sorted: Row) -> Row:
    """Orders and transposes the notes of row2_sorted such that they correspond
    most closely in octave and in pitch to those of row1_original. The notes of
    row1_sorted and row2_sorted are assumed to correspond.
    """

    new_row = []

    for row1_note in row1_original:
        for i, row1_pitch_class in enumerate(row1_sorted):
            if row1_note.same_pitch_class(row1_pitch_class):

                row1_note_midi = row1_note.to_midi()
                new_row_note_midi = row2_sorted[i].to_pitch_class()

                while abs(row1_note_midi - new_row_note_midi) > 6:
                    new_row_note_midi += 12

                new_row.append(LilypondNote.midi_to_lilypond_note(new_row_note_midi))
                continue

    return Row(new_row)


def closest_mapping(row1: Row, row2: Row):
    """Find the closest mapping of the notes of row2 to those of row1."""

    row1_sorted = row1.to_sorted_pitch_class_row()
    row2_sorted = row2.to_sorted_pitch_class_row()
    row2_permutations = permutations(row2_sorted)
    # Set min_deviation to something higher than can be possible
    min_deviation = 24 * len(row1)
    closest_mapping = None

    for sorted_row in row2_permutations:
        current_row = Row(list(sorted_row))
        deviation = row1_sorted.compare_rows(current_row)

        if deviation < min_deviation or closest_mapping is None:
            # Automatically define closest_mapping when otherwise undefined, or
            # when fewer semitones in deviation
            min_deviation = deviation
            closest_mapping = current_row
        elif deviation == min_deviation:
            # If semitones deviation is the same as previous, compare the number
            # of common tones
            previous_common_tones = row1_sorted.common_tones(closest_mapping)
            current_common_tones = row1_sorted.common_tones(current_row)

            if current_common_tones < previous_common_tones:
                min_deviation = deviation
                closest_mapping = current_row
        else:
            continue

    closest_mapping = transpose_row_to_mapping(row1, row1_sorted, closest_mapping)

    return closest_mapping


def cycle_chords(row, num_notes) -> list:
    """Generate music from a row by cycling chords until exactly reaching the
    end of the row.

    >>> f = LilypondNote("f")
    >>> a = LilypondNote("a")
    >>> gs = LilypondNote("gs")
    >>> p = Row([f, a, gs])
    >>> cycle_chords(p, 2)
    [< f a >, < gs f >, < a gs >]
    """

    start_counter = 0
    row_len = len(row)
    chords = []

    while start_counter < lcm(num_notes, row_len):
        current_start_index = start_counter % row_len
        current_end_index = (start_counter + num_notes) % row_len

        if current_end_index < current_start_index:
            # If start index falls after the end index, join two parts
            notes = row[current_start_index:] + row[:current_end_index]
            chords.append(LilypondChord(notes))
        else:
            notes = row[current_start_index:current_end_index]
            chords.append(LilypondChord(notes))

        start_counter += num_notes

    return chords


def main() -> None:
    """The main functionality of the script."""
    num_notes = user.get_num_notes(message = "How many notes in the row? ")
    print("Enter the row:")
    row = user.input_lilypond_row(num_notes)

    chord_size = user.get_num_notes(message = "How many notes in each chord? ")

    music = ""

    for i in range(num_notes):
        chords = map(str, cycle_chords(row, chord_size))
        chords_line = " ".join(chords)
        music += f"{chords_line}\n"
        row = closest_mapping(row, row.transpose_to_next())

    print_lilypond_music(music, increment_filename(FILENAME_FORMAT))


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("\nCtrl-c (keyboard interrupt) pressed, exiting.")
        quit()
