# Author: Richard Davis
# filesystem_resources.py - resources for interacting with filesystem

from shutil import which
import os, re, subprocess

FILENAME_FORMAT = "row_%02d.ly"
DIRNAME = os.path.dirname(__file__)
OUTPUT_DIR = os.path.join(DIRNAME, "out/")

def is_tool(name: str = "") -> bool:
    """Check whether `name` is on PATH and marked as executable."""

    return which(name) is not None


def increment_filename(fmt: str = FILENAME_FORMAT) -> int:
    """Gets filename increment number for new output."""

    i = 0

    while os.path.exists(os.path.join(OUTPUT_DIR, fmt % i)):
        i += 1

    return i


def print_lilypond_music(
        lilypond_music: str,
        file_number: int,
        fmt: str = FILENAME_FORMAT,
        ly_args: list = ["--pdf"],
        gen_midi: bool = False,
) -> None:
    """Print music to lilypond."""

    # Check if lilypond on path
    if not is_tool("lilypond"):
        raise RuntimeError("Lilypond not found when required.")

    # Make output directory if not already present
    if not os.path.isdir(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)

    file_name = os.path.join(OUTPUT_DIR, fmt % file_number)

    lilypond_version = re.split(r' |\\s|\\n', str(subprocess.check_output(["lilypond", "--version"])))[2]

    print(f"Writing music to '{file_name}'")

    with open(file_name, "w") as output_file:
        # Header information
        lilypond_header = f"""\\version "{lilypond_version}"
\\language "english"
\\header {{ tagline = "" }}
\\layout {{
    \\context {{
        \\Score
        \\omit BarNumber
    }}
}}
\\score{{
  {{
    \\omit Staff.BarLine
    \\omit Staff.TimeSignature
    \\accidentalStyle dodecaphonic
"""
        print(lilypond_header, file=output_file)
        print(lilypond_music, file=output_file)

        # Footer information
        print("  }", file=output_file)
        print("  \\layout{}", file=output_file)
        # generate MIDI file if requested, not otherwise
        print("  \\midi{}" if gen_midi else "", file=output_file)
        print("}", file=output_file)

    subprocess.run(["lilypond", "-o", OUTPUT_DIR, *ly_args, file_name])
