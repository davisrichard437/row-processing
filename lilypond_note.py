# Author: Richard Davis
# resources.py - resources for mode generation and printing

import doctest, re
from types import NoneType

MIDINote = int
PitchClass = int

# This regex supports english-style lilypond accidentals (f = flat, s = sharp),
# and no articulations, durations, etc, as they are not processed by this script
# TODO: FIX (multiple accidentals)
LILYPOND_NOTE_REGEX = re.compile(r"[a-g](?:f|s)?(?:(?:,{0,3})|(?:'{0,6}))?")

ACCIDENTALS = [
    "s",
    "f",
]

TRANSPOSITIONS = [
    "'",
    ",",
]

UNTRANSPOSED_MIDI_NOTES = {
    48: "c",
    49: "cs",
    50: "d",
    51: "ef",
    52: "e",
    53: "f",
    54: "fs",
    55: "g",
    56: "af",
    57: "a",
    58: "bf",
    59: "b",
}

# Reverse key value pairs in UNTRANSPOSED_MIDI_NOTES to get pairs of
# "ly_note: midi_note"
UNTRANSPOSED_LILYPOND_NOTES = {v: k for k, v in UNTRANSPOSED_MIDI_NOTES.items()}

PITCH_CLASS_INTS = {k % 12: v for k, v in UNTRANSPOSED_MIDI_NOTES.items()}


class LilypondNote:
    """A class to represent a note in lilypond format."""

    def __init__(self, lilypond_note: str) -> None:
        """Construct a LilypondNote instance with a string representing the note
        as its only attribute.

        >>> a = LilypondNote("a'")
        >>> a.lilypond_note
        "a'"

        Check for invalid lilypond note string format.
        >>> z = LilypondNote('zgfa:')
        Traceback (most recent call last):
        ValueError: invalid lilypond note format.
        """

        if LILYPOND_NOTE_REGEX.match(lilypond_note):
            self.lilypond_note = lilypond_note
        else:
            raise ValueError("invalid lilypond note format.")

    def parse_components(self) -> list[str]:
        """Parse lilypond note and return list of components.

        >>> a = LilypondNote("a'")
        >>> a.parse_components()
        ['a', '', "'"]
        """

        note = self.lilypond_note

        components_list = ["", "", ""]

        # Add note name
        components_list[0] = note[0]

        for c in note[1:]:
            if c in ACCIDENTALS:
                components_list[1] += c
            elif c in TRANSPOSITIONS:
                components_list[2] += c

        return components_list

    def to_midi(self) -> MIDINote:
        """Translate lilypond note to midi note.

        >>> a = LilypondNote("a'")
        >>> a.to_midi()
        69
        """

        components_list = self.parse_components()

        # Initialize midi_note with its absolute, untransposed midi note value
        midi_note = UNTRANSPOSED_LILYPOND_NOTES[components_list[0]]

        # Add 1 for every sharp present
        midi_note += 1 * components_list[1].count(ACCIDENTALS[0])
        # Subtract 1 for every flat present
        midi_note -= 1 * components_list[1].count(ACCIDENTALS[1])

        # Add 12 for every ' present
        midi_note += 12 * components_list[2].count(TRANSPOSITIONS[0])
        # Subtract 12 for every , present
        midi_note -= 12 * components_list[2].count(TRANSPOSITIONS[1])

        return midi_note

    def to_pitch_class(self) -> PitchClass:
        """Translate a lilypond note to a pitch class (integer 0-11).

        >>> a = LilypondNote("a'")
        >>> a.to_pitch_class()
        9
        """

        return self.to_midi() % 12

    def same_pitch_class(self, o) -> bool:
        """Return True if o is same pitch class as self.

        >>> af = LilypondNote("af'")
        >>> gs = LilypondNote("gs'")
        >>> af.same_pitch_class(gs)
        True
        """

        return self.to_pitch_class() == o.to_pitch_class()

    def transpose(self, interval) -> None:
        """Transpose note by interval in semitones.

        >>> note = LilypondNote("a'")
        >>> note.transpose(-9)
        >>> note
        c'
        """

        midi_note = self.to_midi() + interval
        self.lilypond_note = self.midi_to_lilypond_note(midi_note).lilypond_note

    @classmethod
    def midi_to_lilypond_note(cls, midi_note: MIDINote):
        """Translate midi note to lilypond note. Check for valid MIDI lower
        bound.

        >>> LilypondNote.midi_to_lilypond_note(69)
        a'

        >>> LilypondNote.midi_to_lilypond_note(-69)
        Traceback (most recent call last):
        ValueError: -69 is not a valid MIDI note.
        """

        if midi_note < 0:
            raise ValueError(f"{midi_note} is not a valid MIDI note.")

        lilypond_note = ""

        pitch_class_int = midi_note % 12
        pitch_class = PITCH_CLASS_INTS[pitch_class_int]
        lilypond_note += pitch_class

        untransposed_midi_pitch_int = UNTRANSPOSED_LILYPOND_NOTES[pitch_class]

        # Get number of octaves transposed
        transposition = (midi_note - untransposed_midi_pitch_int) // 12

        # Add transposition character according to the number of times transposed
        if transposition > 0:
            lilypond_note += 1 * transposition * TRANSPOSITIONS[0]
        elif transposition < 0:
            lilypond_note += -1 * transposition * TRANSPOSITIONS[1]

        return cls(lilypond_note)

    @classmethod
    def pitch_class_to_lilypond_note(cls, pitch_class: PitchClass):
        """Generate a LilypondNote from a pitch class integer.

        >>> LilypondNote.pitch_class_to_lilypond_note(3)
        ef
        """

        return cls.midi_to_lilypond_note(48 + pitch_class)

    def __str__(self) -> str:
        return self.lilypond_note

    def __repr__(self) -> str:
        return self.__str__()

    def __int__(self) -> int:
        return self.to_midi()

    def __eq__(self, o) -> bool:
        """Test for enharmonic equivalence, not string equivalence.

        >>> a_s = LilypondNote("as")
        >>> b_f = LilypondNote("bf")
        >>> a_s == b_f
        True
        """

        if type(o) is not type(self):
            raise TypeError(f"mismatched types LilypondNote and {type(o)}.")

        return self.to_midi() == o.to_midi()

    def __ne__(self, o) -> bool:
        return not self.__eq__(o)

    def __sub__(self, o) -> int:
        """Return the difference in semitones between two pitches.

        >>> c_s = LilypondNote("cs")
        >>> e = LilypondNote("e")
        >>> e - c_s
        3
        """

        if type(o) is not type(self):
            raise TypeError(f"mismatched types LilypondNote and {type(o)}.")

        return self.to_midi() - o.to_midi()


if __name__ == "__main__":
    doctest.testmod()
