# Row Processing

A simple python script I made to help with a serial computer-assisted composition problem.
If you need it to do anything else, edit the `main` function in the `main.py` file.

## Usage

```
git clone https://gitlab.com/davisrichard437/row-processing.git
cd row-processing
python3 main.py
```

Follow the instructions; use absolute-pitch lilypond notes.
